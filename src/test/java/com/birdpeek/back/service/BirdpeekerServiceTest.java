package com.birdpeek.back.service;

import com.birdpeek.back.model.Birdpeeker;
import com.birdpeek.back.repository.BirdpeekerRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BirdpeekerServiceTest {

    @Mock
    private BirdpeekerRepository repository;

    @InjectMocks
    private BirdpeekerService service;

    @Test
    @DisplayName("Should add a new birdpeeker")
    void shouldAddNewBirdpeeker() {
        Birdpeeker birdpeeker = createBirdpeeker();
        when(repository.save(birdpeeker)).thenReturn(birdpeeker);
        assertEquals(service.save(birdpeeker), birdpeeker);
    }

    @Test
    @DisplayName("Should find a birdpeeker by its ID")
    void shouldFindBirdpeekerById() throws ChangeSetPersister.NotFoundException {
        Birdpeeker birdpeeker = createBirdpeeker();
        when(repository.findById(1L)).thenReturn(Optional.of(birdpeeker));
        Birdpeeker found = service.findById(1L);
        assertEquals(found, birdpeeker);
        assertEquals(found.getUsername(), birdpeeker.getUsername());
    }

    @Test
    @DisplayName("Should throw exception if doesn't find a birdpeeker by its ID")
    void shouldntFindBirdpeekerById() {
        assertThrows(ChangeSetPersister.NotFoundException.class,() -> service.findById(any()));
    }

    private Birdpeeker createBirdpeeker() {
        return new Birdpeeker("Rebecca", "Souza", "re_souza", "resouza@gmail.com", "123456", "https://www.fotodarebecca.com.br/picture", "1996-10-31");
    }

}
