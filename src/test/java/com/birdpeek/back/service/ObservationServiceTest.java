package com.birdpeek.back.service;

import com.birdpeek.back.model.Observation;
import com.birdpeek.back.repository.ObservationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ObservationServiceTest {

    @Mock
    private ObservationRepository repository;

    @InjectMocks
    private ObservationService service;

    @Test
    @DisplayName("Should add a new observation")
    void shouldAddNewObservation() {
        Observation observation = createObservation();
        when(repository.save(observation)).thenReturn(observation);
        assertEquals(service.save(observation), observation);
    }

    @Test
    @DisplayName("Should find an observation by its ID")
    void shouldFindBirdById() throws ChangeSetPersister.NotFoundException {
        Observation observation = createObservation();
        when(repository.findById(1L)).thenReturn(Optional.of(observation));
        assertEquals(service.getById(1L), observation);
        assertEquals(service.getById(1L).getBirdObs(), observation.getBirdObs());
    }

    @Test
    @DisplayName("Should throw exception if doesn't find an observation by its ID")
    void shouldntFindObservationById() {
        assertThrows(ChangeSetPersister.NotFoundException.class,() -> service.getById(any()));
    }

    @Test
    @DisplayName("Should find all observations")
    void shouldFindAll() {
        List<Observation> observations = List.of(createObservation());
        when(repository.findAll()).thenReturn(observations);
        assertEquals(service.getAll(), observations);
    }

    @Test
    @DisplayName("Should update an observation")
    void shouldUpdateObservation() throws Exception {
        Observation observation = createObservation();
        Observation updatedObservation = createObservation();
        updatedObservation.setBirdObs("Pintassilgo");
        when(repository.findById(observation.getIdObs())).thenReturn(Optional.of(observation));
        when(repository.save(observation)).thenReturn(updatedObservation);
        assertEquals(service.updateObservation(observation), updatedObservation);
    }

    @Test
    @DisplayName("Should throw exception if doesn't find an observation by its ID in update")
    void shouldThrowExceptionNotFoundWhenUpdate() {
        Observation observation = createObservation();
        assertThrows(Exception.class,() -> service.updateObservation(observation));
    }

    @Test
    @DisplayName("Should delete an observation by its ID")
    void shouldDeleteObservationById() {
        service.deleteById(1L);
        verify(repository, times(1)).deleteById(1L);
    }

    private Observation createObservation() {
        return new Observation("Rolinha", "2022-10-28", "10:56", "Quintal de casa");
    }

}
