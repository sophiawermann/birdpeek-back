package com.birdpeek.back.service;

import com.birdpeek.back.model.Bird;
import com.birdpeek.back.repository.BirdRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BirdServiceTest {

    @Mock
    private BirdRepository repository;

    @InjectMocks
    private BirdService service;

    @Test
    @DisplayName("Should add a new bird")
    void shouldAddNewBird() {
        Bird bird = createBird();
        when(repository.save(bird)).thenReturn(bird);
        assertEquals(service.save(bird), bird);
    }

    @Test
    @DisplayName("Should find a bird by its ID")
    void shouldFindBirdById() throws ChangeSetPersister.NotFoundException {
        Bird bird = createBird();
        when(repository.findById(1L)).thenReturn(Optional.of(bird));
        assertEquals(service.getById(1L), bird);
        assertEquals(service.getById(1L).getName(), bird.getName());
    }

    @Test
    @DisplayName("Should throw exception if doesn't find a bird by its ID")
    void shouldntFindBirdById() {
        assertThrows(ChangeSetPersister.NotFoundException.class,() -> service.getById(2L));
    }

    @Test
    @DisplayName("Should find bird by its portuguese or english name")
    void shouldFindBirdByNamePtEn() {
        Bird bird = createBird();
        when(repository.findByNamePtEnLike("Rolinha")).thenReturn(List.of(bird));
        assertEquals(service.findByNamePtEn("Rolinha"), List.of(bird));
        assertThat(service.findByNamePtEn("Rolinha")).contains(bird);
    }

    @Test
    @DisplayName("Should find all birds")
    void shouldFindAll() {
        List<Bird> birds = List.of(createBird());
        when(repository.findAll()).thenReturn(birds);
        assertEquals(service.getAll(), birds);
    }

    @Test
    @DisplayName("Should update a bird")
    void shouldUpdateBird() throws Exception {
        Bird bird = createBird();
        Bird updatedBird = createBird();
        updatedBird.setEnglishName("want-want");
        when(repository.findById(bird.getId())).thenReturn(Optional.of(bird));
        when(repository.save(bird)).thenReturn(updatedBird);
        assertEquals(service.updateBird(bird), updatedBird);
    }

    @Test
    @DisplayName("Should throw exception if doesn't find a bird by its ID in update")
    void shouldThrowExceptionNotFoundWhenUpdate() {
        Bird bird = createBird();
        assertThrows(ChangeSetPersister.NotFoundException.class,() -> service.updateBird(bird));
    }

    @Test
    @DisplayName("Should delete a bird by its ID")
    void shouldDeleteBirdById() {
        service.deleteById(1L);
        verify(repository, times(1)).deleteById(1L);
    }

    private Bird createBird() {
        return new Bird("Rolinha", "scientificus nominus", "little roll", "URL", "Day", "Fruits", "No");
    }

}