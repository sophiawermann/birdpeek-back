package com.birdpeek.back.controller;

import com.birdpeek.back.model.Bird;
import com.birdpeek.back.service.BirdService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BirdController.class)
class BirdControllerTest {

    private final ObjectMapper mapper = new ObjectMapper();
    private static final long TEST_ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BirdService service;

    @Test
    @DisplayName("Should save a new bird")
    void shouldSaveNewBird() throws Exception {
        Bird birdo = createBird();
        when(service.save(birdo)).thenReturn(birdo);
        String saveBird = mapper.writeValueAsString(birdo);
        mockMvc.perform(post("/bird")
                .contentType(MediaType.APPLICATION_JSON)
                .content(saveBird))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Shound find bird by ID")
    void shouldFindById() throws Exception {
        when(service.getById(TEST_ID)).thenReturn(createBird());
        String findId = mockMvc.perform(get("/bird/" + TEST_ID))
                .andExpect(status().isOk())
                .andReturn().getResponse()
                .getContentAsString();
        assertEquals(findId, new ObjectMapper().writeValueAsString(createBird()));
    }

    @Test
    @DisplayName("Should return a list with all birds")
    void shouldFindAllBirds() throws Exception {
        List<Bird> birds = List.of(createBird());
        when(service.getAll()).thenReturn(birds);
        String listOfBirds = mockMvc.perform(get("/bird"))
                .andExpect(status().isOk())
                .andReturn().getResponse()
                .getContentAsString();
        assertEquals(listOfBirds, new ObjectMapper().writeValueAsString(birds));
    }

    @Test
    @DisplayName("Should find a bird by portuguese or english name")
    void ShouldFindByNamePtEn() throws Exception {
        List<Bird> birds = List.of(createBird());
        when(this.service.findByNamePtEn("roli")).thenReturn(birds);
        String birdoPtEn = mockMvc.perform(get("/bird/search/roli"))
                .andExpect(status().isOk())
                .andReturn().getResponse()
                .getContentAsString();
        assertEquals(birdoPtEn, mapper.writeValueAsString(birds));
    }

    @Test
    @DisplayName("Should update bird information")
    void shouldUpdateBird() throws Exception {
        when(service.updateBird(createBird())).thenReturn(createBird());
        String updateBird = mapper.writeValueAsString(createBird());
        mockMvc.perform(put("/bird/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateBird))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should delete a bird by ID")
    void shouldDeleteById() throws Exception {
        mockMvc.perform(delete("/bird/" + TEST_ID))
                .andExpect(status().isNoContent());
        verify(service, times(1)).deleteById(TEST_ID);
    }

    private Bird createBird() {
        return new Bird("Rolinha", "scientificus nominus", "little roll", "URL", "Day", "Fruits", "No");
    }
}