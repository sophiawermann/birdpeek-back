package com.birdpeek.back.service;

import com.birdpeek.back.model.Birdpeeker;
import com.birdpeek.back.repository.BirdpeekerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BirdpeekerService {

    @Autowired
    private BirdpeekerRepository repository;

    public Birdpeeker findById(Long id) throws ChangeSetPersister.NotFoundException {
        Optional<Birdpeeker> user = this.repository.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public Birdpeeker save(Birdpeeker birdpeeker) {
        return this.repository.save(birdpeeker);
    }
}
