package com.birdpeek.back.service;

import com.birdpeek.back.model.Observation;
import com.birdpeek.back.repository.ObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ObservationService {

    @Autowired
    private ObservationRepository repository;

    public Observation save(Observation observation) {
        return this.repository.save(observation);
    }

    public List<Observation> getAll() {
        return (List<Observation>) this.repository.findAll();
    }

    public Observation getById(Long idObs) throws ChangeSetPersister.NotFoundException {
        Optional<Observation> observation = this.repository.findById(idObs);
        if (observation.isPresent()) {
            return observation.get();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public Observation updateObservation(Observation observation) throws Exception {
        Optional<Observation> update = this.repository.findById(observation.getIdObs());
        if (update.isPresent()) {
            update.get().setBirdObs(observation.getBirdObs());
            update.get().setDateObs(observation.getDateObs());
            update.get().setTimeObs(observation.getTimeObs());
            update.get().setLocationObs(observation.getLocationObs());
            return this.repository.save(update.get());
        } else {
            throw new Exception("Oops! Something went wrong, try again :(");
        }
    }

    public void deleteById(Long idObs) {
        this.repository.deleteById(idObs);
    }

}
