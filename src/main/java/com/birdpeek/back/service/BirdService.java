package com.birdpeek.back.service;

import com.birdpeek.back.model.Bird;
import com.birdpeek.back.repository.BirdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BirdService {

    @Autowired
    private BirdRepository repository;

    public Bird save(Bird bird) {
        return this.repository.save(bird);
    }

    public List<Bird> getAll() {
        return (List<Bird>) this.repository.findAll();
    }

    public Bird getById(Long id) throws ChangeSetPersister.NotFoundException {
        Optional<Bird> bird = this.repository.findById(id);
        if (bird.isPresent()) {
            return bird.get();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public List<Bird> findByNamePtEn(String search) {
        return this.repository.findByNamePtEnLike(search);
    }

    public Bird updateBird(Bird bird) throws ChangeSetPersister.NotFoundException {
        Optional<Bird> update = this.repository.findById(bird.getId());
        if (update.isPresent()) {
            update.get().setName(bird.getName());
            update.get().setScientificName(bird.getScientificName());
            update.get().setEnglishName(bird.getEnglishName());
            update.get().setImage(bird.getImage());
            update.get().setActivity(bird.getActivity());
            update.get().setEatingHabits(bird.getEatingHabits());
            update.get().setMigration(bird.getMigration());
            return this.repository.save(update.get());
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public void deleteById(Long id) {
        this.repository.deleteById(id);
    }

}
