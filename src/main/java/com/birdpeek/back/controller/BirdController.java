package com.birdpeek.back.controller;

import com.birdpeek.back.model.Bird;
import com.birdpeek.back.service.BirdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bird")
@CrossOrigin
public class BirdController {

    @Autowired
    private BirdService birdService;

    @GetMapping
    ResponseEntity<List<Bird>> getAll() {
        return ResponseEntity.ok(this.birdService.getAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<Bird> getById(@PathVariable Long id) throws ChangeSetPersister.NotFoundException {
        return ResponseEntity.ok(birdService.getById(id));
    }

    @GetMapping("/search/{search}")
    ResponseEntity<List<Bird>> findByNamePtEn(@PathVariable String search) {
        return ResponseEntity.ok(this.birdService.findByNamePtEn(search));
    }

    @PostMapping
    ResponseEntity<Bird> save(@RequestBody Bird bird) {
        return new ResponseEntity<>(this.birdService.save(bird), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    ResponseEntity<Bird> updateBird(@RequestBody Bird bird) throws Exception {
        return ResponseEntity.ok(this.birdService.updateBird(bird));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Bird> deleteById(@PathVariable Long id) {
        this.birdService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
