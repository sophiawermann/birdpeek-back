package com.birdpeek.back.controller;

import com.birdpeek.back.model.Birdpeeker;
import com.birdpeek.back.service.BirdpeekerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private BirdpeekerService service;

    @GetMapping("/{id}")
    ResponseEntity<Birdpeeker> findById(@PathVariable Long id) throws ChangeSetPersister.NotFoundException {
        return ResponseEntity.ok(service.findById(id));
    }

    @PostMapping
    ResponseEntity<Birdpeeker> save(@RequestBody Birdpeeker birdpeeker) {
        return new ResponseEntity<>(this.service.save(birdpeeker), HttpStatus.CREATED);
    }
}
