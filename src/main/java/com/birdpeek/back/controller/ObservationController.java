package com.birdpeek.back.controller;

import com.birdpeek.back.model.Bird;
import com.birdpeek.back.model.Observation;
import com.birdpeek.back.service.ObservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/observation")
@CrossOrigin
public class ObservationController {

    @Autowired
    private ObservationService observationService;

    @GetMapping
    ResponseEntity<List<Observation>> getAll() {
        return ResponseEntity.ok(this.observationService.getAll());
    }

    @GetMapping("/{idObs}")
    ResponseEntity<Observation> getById(@PathVariable Long idObs) throws ChangeSetPersister.NotFoundException {
        return ResponseEntity.ok(observationService.getById(idObs));
    }

    @PostMapping
    ResponseEntity<Observation> save(@RequestBody Observation observation) {
        return new ResponseEntity<>(this.observationService.save(observation), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    ResponseEntity<Observation> updateObservation(@RequestBody Observation observation) throws Exception {
        return ResponseEntity.ok(this.observationService.updateObservation(observation));
    }

    @DeleteMapping("/{idObs}")
    ResponseEntity<Observation> deleteById(@PathVariable Long idObs) {
        this.observationService.deleteById(idObs);
        return ResponseEntity.noContent().build();
    }

}
