package com.birdpeek.back.model;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class Bird {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SCIENTIFIC_NAME")
    private String scientificName;

    @Column(name = "ENGLISH_NAME")
    private String englishName;

    @Column(name = "IMAGE")
    private String image;

    @Column(name = "ACTIVITY")
    private String activity;

    @Column(name = "EATING_HABITS")
    private String eatingHabits;

    @Column(name = "MIGRATION")
    private String migration;

    public Bird() {}

    public Bird(String name, String scientificName, String englishName, String image, String activity, String eatingHabits, String migration) {
        this.name = name;
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.image = image;
        this.activity = activity;
        this.eatingHabits = eatingHabits;
        this.migration = migration;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setEatingHabits(String eatingHabits) {
        this.eatingHabits = eatingHabits;
    }

    public void setMigration(String migration) {
        this.migration = migration;
    }
}
