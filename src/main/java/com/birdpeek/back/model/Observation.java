package com.birdpeek.back.model;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class Observation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long idObs;

    @Column(name = "BIRD")
    private String birdObs;

    @Column(name = "DATE")
    private String dateObs;

    @Column(name = "TIME")
    private String timeObs;

    @Column(name = "LOCATION")
    private String locationObs;

    public Observation() {}

    public Observation(String birdObs, String dateObs, String timeObs, String locationObs) {
        this.birdObs = birdObs;
        this.dateObs = dateObs;
        this.timeObs = timeObs;
        this.locationObs = locationObs;
    }

    public void setBirdObs(String birdObs) {
        this.birdObs = birdObs;
    }

    public void setDateObs(String dateObs) {
        this.dateObs = dateObs;
    }

    public void setTimeObs(String timeObs) {
        this.timeObs = timeObs;
    }

    public void setLocationObs(String locationObs) {
        this.locationObs = locationObs;
    }

}
