package com.birdpeek.back.repository;

import com.birdpeek.back.model.Observation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationRepository extends CrudRepository<Observation, Long> {

    Observation save(Observation observation);

}
