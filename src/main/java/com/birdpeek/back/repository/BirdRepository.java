package com.birdpeek.back.repository;

import com.birdpeek.back.model.Bird;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BirdRepository extends CrudRepository<Bird, Long> {

    @Query("select b from Bird b where b.name like %:search% or b.englishName like %:search%")
    List<Bird> findByNamePtEnLike(@Param("search") String search);
}

