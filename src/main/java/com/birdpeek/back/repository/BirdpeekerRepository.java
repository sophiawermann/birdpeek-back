package com.birdpeek.back.repository;

import com.birdpeek.back.model.Birdpeeker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BirdpeekerRepository extends CrudRepository<Birdpeeker, Long> {
}
