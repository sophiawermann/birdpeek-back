<h1 align="center">
        <img alt="Banner" title="#Banner" style="object-fit: cover; height:250px;" src="readmebanner.png"/>
</h1>


<div>
  <img src="https://img.shields.io/badge/web%3F-ok-yellow?style=for-the-badge" alt="Sistema web Ok" />
  <img src="https://img.shields.io/badge/server%3F-ok-yellow?style=for-the-badge" alt="Server Ok" />
</div>

## 📌 Index

<div align="center">
  <a href="#-index"> Index </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#about-the-project"> About the project </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-layout"> Layout </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-technologies"> Technologies </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#checkered_flag-project-status"> Project status </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#computer-running-locally"> Running locally </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#bar_chart-database-configuration"> Database configuration </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#memo-authors"> Authors </a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-contributions"> Contributions </a> 
</div>

## About the project 

This project was developed for a challenge proposed during the training phase of the **StartDB** internship program and is divided into two main parts: [frontend](https://gitlab.com/bisakaniwa/birdpeek-front) and [backend](https://gitlab.com/sophiawermann/birdpeek-back).

### 🎯 Challenge

The description below indicates the information available for the system that should be developed.

>“Amateur ornithologists are interested in birds, especially small birds. The ornithologist uses binoculars or other equipment to sight and perhaps photograph birds. The ornithologist is interested in seeing birds in their natural environment, so he writes down where the bird was seen and when it was seen. With this data, he plans activities with other ornithologists who want to observe this bird. The ornithologist uses specialized books, in this case, the book that our client uses is “Birds that we see in the fields” by Fontana et alli. With the guide in hand, our client sees and identifies the bird and wants to write down when and where the occurrence happened.
>
>The guide presents a wealth of information about the birds, including names in Portuguese, English and Latin. Other characteristics include size, gender, predominant color, family and habitat. In some cases, the ornithologist consults the book while observing the bird. In this case, he can inform the page and the row and column where the bird is registered. In other cases, the ornithologist remembers part of the name, color or habitat and needs help locating the bird in the catalogue. Each annotation is composed of date, time, place and identified bird.
>
>What is expected of the developed system? It is expected that at least it will be possible to carry out the listing/filtering of birds from the catalogs (it is the task of the group to decide what information will be part of the system based on what is used by the catalogs and similar systems). As it is a challenge, additional functionalities such as a page for registering offers, a page for confirming a reservation, etc., will be an interesting demonstration of the group's capabilities.”

### :hatching_chick: Birdpeek           

Birdpeek is a web application designed for those who love to **peek at birds**! On birdpeek website you can catalog birds of interest and register notes about the birds that you have peeked at. :telescope:

## 📸 Layout

Prototypes of the web application pages were developed on Figma and are available at <https://www.figma.com/file/sbPfSombkRjTryWyGgFf34/Birdpeek-UI>

## 🛠 Technologies

The following tools were used in this project:

- [Java](https://dev.java/learn/) - OpenJDK 17
- [Gradle](https://docs.gradle.org/current/userguide/userguide.html) - 7.5.1
- [Spring Boot](https://spring.io/learn) - 2.7.4
- [JUnit](https://junit.org/junit5/docs/current/user-guide/) 
- [TypeScript](https://www.typescriptlang.org/docs/) - 4.7.4
- [Angular](https://angular.io/docs) - 14.2.6
- [Jest](https://jestjs.io/docs/getting-started) 

## :checkered_flag: Project status

The website already has several functionalities implemented, but it is under development. The authors intend to add a login system, in order to limit access to some pages only to registered users. Improvement is continuous!

## :computer: Running locally

Birdpeek-back is a Spring Boot application built using Gradle. You can run it from the command line:

```
git clone https://gitlab.com/sophiawermann/birdpeek-back
cd birdpeek-back
./gradlew build
```

You can then access Birdpeek-back here: <http://localhost:8080/>

## :bar_chart: Database configuration

In its default configuration, Birdpeek-back uses MySQL as persistent database. You could start MySQL locally with whatever installer works for your OS. Information about download and installation can be found [here](https://dev.mysql.com/downloads/).

## :memo: Authors

Here you can see the team responsible for this entire project:

<div>
  <div>
    <table style="width:50%">
      <tr align=center>
        <th><strong>Bianca Sakaniwa</strong></th>
      </tr>
      <tr align=center>
        <td>
          <a href="https://www.linkedin.com/in/bianca-sakaniwa/">
            <img width="200" height="180" style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/103525436?v=4">
          </a>
        </td>
      </tr>
    </table>
  </div>
  <div>
    <table style="width:50%">
      <tr align=center>
        <th><strong>Sophia Wermann</strong></th>
      </tr>
      <tr align=center>
        <td>
          <a href="https://www.linkedin.com/in/sophia-wermann/">
            <img width="200" height="180" style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/81588219?v=4">
          </a>
        </td>
      </tr>
    </table>
  </div>
</div>

## 🤝 Contributions 

Follow the steps below to contribute:

1. *Fork* the project (<https://gitlab.com/sophiawermann/birdpeek-back>)
2. Clone your *fork* to your machine (`git clone https://github.com/user_name/REPO_NAME.git`)
3. Create a *branch* to perform your modification (`git checkout -b feature/name_new_feature`)
4. Add your modifications and *commit* (`git commit -m "Describe your modification"`)
5. *Push* (`git push origin feature/name_new_feature`)
6. Create a new *Pull Request*
7. Okay, now just wait for the analysis

> If you have any questions, check out this [guide](https://github.com/firstcontributions/first-contributions)


------------------------


Project published in 2022           
Made with :purple_heart: by Bianca and Sophia           
Liked? Leave a little star to help the project ⭐           



- [Back to Top](#-index)
